package in.co.nebulaX.foofighter;

import in.co.nebulaX.foofighter.SensorActivity.MyCanvas;

import java.io.IOException;
import java.util.Random;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

public class CameraOverlayActivity extends Activity implements SensorEventListener {

	SensorManager mSensorManager;
	MyCanvas ourSurfaceView;

	Sensor accelerometer;
	Sensor magnetometer;

	Camera mCamera;
	
	float azimuth, pitch, roll;

	float[] mGravity;
	float[] mGeomagnetic;


	//int counter = 0;
	int timeCounter = 0;
	
	float pos1a, pos1b, pos1c, pos2a, pos2b, pos2c;
	Random random;

	Bitmap zombie , blood;
	Paint paint;
	int threshold = 1800;
	
	int points = 0;
	BitmapScaler scaler;
	// ImageView iv;
	int newWidth = 500; //50
	
	public boolean isTouched=false;
	public boolean isZombieShowing;	
	public boolean zombieKilled = false;

	public boolean gameOver = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		if (mSensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER).size() != 0
				&& mSensorManager.getSensorList(Sensor.TYPE_MAGNETIC_FIELD)
						.size() != 0) {
			accelerometer = mSensorManager
					.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
			magnetometer = mSensorManager
					.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		} else {
			Toast.makeText(this, "Hardware not supported", Toast.LENGTH_SHORT)
					.show();
		}
		random = new Random();
		zombieInit();
	
		blood = BitmapFactory.decodeResource(getResources(), R.drawable.blood);
		try {
			scaler = new BitmapScaler(getResources() , R.drawable.ghost , newWidth);
		} catch (IOException e) {
			e.printStackTrace();
		}
//		try {
//			for(int i=0;i<10;i++){
//				scaler[i]= new BitmapScaler(getResources() , R.drawable.ghost , newWidth);
//				newWidth+=10;
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
		zombie = BitmapFactory
				.decodeResource(getResources(), R.drawable.zombie);
		
		 paint = new Paint(); 
		// Animation scale = new ScaleAnimation(1, 5, 1, 5,
		// Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
		// 0.5f);
		// scale.setDuration(10000);
		// iv.setAnimation(scale);

		Preview mPreview = new Preview(this);
		DrawOnTop mDraw = new DrawOnTop(this);
		mDraw.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				gameOver =false;
				if(isZombieShowing){
					zombieKilled = true;
					points++;
					//zombieInit();
				}
				Log.v("Touch" , "Entered onTouch of mDraw class");
				return false;
			}
		});

		setContentView(mPreview);
		addContentView(mDraw, new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT));

	}

	@Override
	protected void onPause() {

		super.onPause();
		mSensorManager.unregisterListener(this);	
	}

	@Override
	protected void onResume() {

		super.onResume();
		mSensorManager.registerListener(this, accelerometer,
				SensorManager.SENSOR_DELAY_UI);
		mSensorManager.registerListener(this, magnetometer,
				SensorManager.SENSOR_DELAY_UI);
	}

//	public static Bitmap scaleBitmap(Bitmap bitmap, int wantedWidth,
//			int wantedHeight) {
//		Bitmap newBitmap = Bitmap.createScaledBitmap(bitmap, wantedWidth,
//				wantedHeight, false);
//		return newBitmap;
//	}
	
//	int height = 100, width = 100;

	class DrawOnTop extends View{


		public DrawOnTop(Context context) {
			super(context);
			
		}

		@SuppressLint("DrawAllocation")
		@Override
		protected void onDraw(Canvas canvas) {

			timeCounter ++;
			isZombieShowing = false;
			boolean randomvariable = true;
            paint.setStyle(Paint.Style.FILL); 
            paint.setColor(Color.RED); 
            paint.setTextSize(100);
            canvas.drawText("SCORE : "+points, 30, 100, paint); 

			if(timeCounter == 20 ){
				timeCounter = 0 ;
			threshold--;
			if(threshold==1000){
				threshold = 1800;
				showCustomDialog();
			}
			}
			
            Rect fill = new Rect(1000 , 30 , threshold , 130);
            Rect empty = new Rect(threshold , 30 , 1800 , 130);
            
            paint.setColor(Color.rgb(34, 139, 34));
            paint.setStyle(Paint.Style.FILL_AND_STROKE);
            canvas.drawRect(fill, paint);
            
            paint.setStyle(Paint.Style.STROKE);
            canvas.drawRect(empty, paint);
			
            if(zombieKilled){
            	// Add blood bitmap here
            	// TODO And add sound
            	//canvas.drawText("ZOMBIE-KILLED" , 10 ,50, paint);
            	canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
            	canvas.drawBitmap(blood, (canvas.getWidth()/2)-(blood.getWidth()/2)
						, (canvas.getHeight()/2)-(blood.getHeight()/2), null);
            	try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
            	zombieKilled = false;
            	threshold+=5;
            	randomvariable = false;
            	zombieInit();
            }
            
			if (isClose() && randomvariable) {

				//for (int i=0 ; i<10 ; i++){
				canvas.drawBitmap(scaler.getScaled(), (canvas.getWidth()/2)-(zombie.getWidth()/2)
						, (canvas.getHeight()/2)-(zombie.getHeight()/2), null);
				isZombieShowing = true;
				//}
			}				
			
			invalidate();
			super.onDraw(canvas);
		}

	}

	class Preview extends SurfaceView implements SurfaceHolder.Callback {
		SurfaceHolder mHolder;
		

		@SuppressWarnings("deprecation")
		Preview(Context context) {
			super(context);

			// Install a SurfaceHolder.Callback so we get notified when

			// underlying surface is created and destroyed.
			mHolder = getHolder();
			mHolder.addCallback(this);
			mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		}

		public void surfaceCreated(SurfaceHolder holder) {
			// The Surface has been created, acquire the camera and tell
			// to draw.
			mCamera = Camera.open();
			try {
				mCamera.setPreviewDisplay(holder);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		public void surfaceDestroyed(SurfaceHolder holder) {
			// Surface will be destroyed when we return, so stop the

			// Because the CameraDevice object is not a shared resource,
			// important to release it when the activity is paused.
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
			
		}

		public void surfaceChanged(SurfaceHolder holder, int format, int w,
				int h) {
			// Now that the size is known, set up the camera parameters
			// the preview.
			Camera.Parameters parameters = mCamera.getParameters();
			parameters.setPreviewSize(w, h);
			mCamera.setParameters(parameters);
			mCamera.startPreview();

		}


	}

	public void zombieInit() {

		// To initialise a zombie at a random azimuth and roll position

		pos1a = (-3 + 6 * random.nextFloat());
		pos1b = (-3 + 6 * random.nextFloat());
		pos1c = (-3 + 6 * random.nextFloat());

		pos2a = (-3 + 6 * random.nextFloat());
		pos2b = (-3 + 6 * random.nextFloat());
		pos2c = (-3 + 6 * random.nextFloat());
		 

	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

	@Override
	public void onSensorChanged(SensorEvent event) {

		try {
			Thread.sleep(16);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
			mGravity = event.values;
		if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
			mGeomagnetic = event.values;
		if (mGravity != null && mGeomagnetic != null) {
			float R[] = new float[9];
			float I[] = new float[9];
			boolean success = SensorManager.getRotationMatrix(R, I, mGravity,
					mGeomagnetic);
			if (success) {
				float orientation[] = new float[3];
				SensorManager.getOrientation(R, orientation);

				azimuth = orientation[0];
				pitch = orientation[1];
				roll = orientation[2];

				// Log.v("orientation", "azimuth :" + (int) (azimuth * 1000));
				// Log.v("orientation" , "pitch :"+pitch);
				// Log.v("orientation", "roll :" + (int) (roll * 1000));
				// orientation contains: azimut, pitch and roll
			}
		}

	}

	public boolean isClose() {

		int counter1 = 0, counter2 = 0;
		float error = 0.5f;

		if (Math.abs(Math.abs(pos1a) - Math.abs(azimuth)) <= error) {
			counter1++;
		}
		if (Math.abs(Math.abs(pos1b) - Math.abs(pitch)) <= error) {
			counter1++;
		}
		if (Math.abs(Math.abs(pos1c) - Math.abs(roll)) <= error) {
			counter1++;
		}

		if (Math.abs(Math.abs(pos2a) - Math.abs(azimuth)) <= error) {
			counter2++;
		}
		if (Math.abs(Math.abs(pos2b) - Math.abs(pitch)) <= error) {
			counter2++;
		}
		if (Math.abs(Math.abs(pos2c) - Math.abs(roll)) <= error) {
			counter2++;
		}

		if (counter1 == 2 || counter2 == 2) {
			return true;
		} else {
			return false;
		}
	}

	public void showCustomDialog(){
	
		Toast.makeText(this, "YOUR SCORE : "+points, 
				Toast.LENGTH_LONG).show();
		gameOver = true;
		points = 0;
	}
}
