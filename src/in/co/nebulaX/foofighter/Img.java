//package in.co.nebulaX.foofighter;
//
//import android.content.Context;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.graphics.Matrix;
//  
//public class Img {
//  
// Bitmap img;
// int w; // width
// int h; // height
// public static Context c;
//  
// Img(Context _c, int _img, int _width) {
//  
// Bitmap sampleImg = BitmapFactory.decodeResource(_c.getResources(), _img);
//  
// // Get the source image's dimensions
// BitmapFactory.Options options = new BitmapFactory.Options();
// options.inJustDecodeBounds = true;
// BitmapFactory.decodeResource(_c.getResources(), _img, options);
//  
// int srcWidth = options.outWidth;
//  
// float desiredScale = (float) _width / srcWidth;
//  
// // Decode with inSampleSize
// options.inJustDecodeBounds = false;
// options.inDither = false;
// options.inSampleSize = 1;
// options.inScaled = false;
// options.inPreferredConfig = Bitmap.Config.ARGB_8888;
// BitmapFactory.decodeResource(_c.getResources(), _img, options);
//  
// // Resize
// Matrix matrix = new Matrix();
// matrix.postScale(desiredScale, desiredScale);
// img = Bitmap.createBitmap(sampleImg, 0, 0, sampleImg.getWidth(), sampleImg.getHeight(), matrix, true);
// sampleImg = null;
//  
// w = img.getWidth();
// h = img.getHeight();
//  
// }
//}
