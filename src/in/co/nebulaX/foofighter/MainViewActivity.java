//package in.co.nebulaX.foofighter;
//
//import java.util.Random;
//import android.app.Activity;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.graphics.Canvas;
//import android.hardware.Sensor;
//import android.hardware.SensorEvent;
//import android.hardware.SensorEventListener;
//import android.hardware.SensorManager;
//import android.os.BatteryManager;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.MotionEvent;
//import android.view.SurfaceHolder;
//import android.view.SurfaceView;
//import android.view.View;
//import android.view.View.OnTouchListener;
//import android.view.Window;
//import android.view.WindowManager;
//import android.widget.Toast;
//
//public class MainViewActivity extends Activity implements 
//											SensorEventListener , OnTouchListener{
//	
//	MyCanvas ourSurfaceView;
//	Canvas canvas;
//	
//	SensorManager mSensorManager;
//	Sensor accelerometer;
//	
//	float sensorX , sensorY , sensorZ ;
//	
//	float posX , posY , posZ ;
//	
//	Bitmap zombie , blood;
//	public boolean isZombieShowing;
//	
//	public class MyCanvas extends SurfaceView implements Runnable{
//
//		SurfaceHolder holder;
//		Thread thread = null;
//		boolean isRunning = false;
//
//		public MyCanvas(Context context) {
//			super(context);
//			holder = getHolder();
//		}
//
//		public void pause() {
//			isRunning = false;
//			while (true) {
//				try {
//					thread.join();
//					break;
//				} catch (InterruptedException e) {
//					e.printStackTrace();
//				}
//			}
//			thread = null;
//		}
//
//		public void resume() {
//			isRunning = true;
//			thread = new Thread(this);
//			thread.start();
//		}
//
//		@Override
//		public void run() {
//
//			while (isRunning) {
//				if (!holder.getSurface().isValid())
//					continue;
//
//				canvas = holder.lockCanvas();
//				canvas.drawRGB(200, 0, 0);
//				isZombieShowing = false;
//				if(isClose())
//				{
//					canvas.drawBitmap(zombie, 0,0, null);
//					isZombieShowing = true;
//				}
//				
////				if(isZombieShowing && isFiring(getApplicationContext())){
////					zombieKill();
////				}
//				
//				holder.unlockCanvasAndPost(canvas);
//			}
//		}
//	}
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		
//		super.onCreate(savedInstanceState);
//		
//		 requestWindowFeature(Window.FEATURE_NO_TITLE);
//		  getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN , 
//				  WindowManager.LayoutParams.FLAG_FULLSCREEN);
//		  
//		  mSensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
//		  if(mSensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER).size()!=0){
//			 
//			  accelerometer = mSensorManager.
//					  getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
//			  
//		  }else {
//				Toast.makeText(this, "Hardware not supported", Toast.LENGTH_SHORT)
//				.show();
//				}
//		  
//		  	zombieInit();
//		  	zombie = BitmapFactory.
//		  			decodeResource(getResources(), R.drawable.zombie);
//		  	
//		  	ourSurfaceView = new MyCanvas(this);
//			ourSurfaceView.setOnTouchListener(this);
//			ourSurfaceView.resume();
//			setContentView(ourSurfaceView);
//	}
//
//	@Override
//	protected void onPause() {
//		
//		super.onPause();
//		mSensorManager.unregisterListener(this);
//	}
//
//	@Override
//	protected void onResume() {
//		
//		super.onResume();
//		mSensorManager.registerListener(this, accelerometer,
//				SensorManager.SENSOR_DELAY_UI);
//	}
//
//	@Override
//	public void onAccuracyChanged(Sensor sensor, int accuracy) {
//		
//	}
//
//	@Override
//	public void onSensorChanged(SensorEvent event) {
//		
//		try {
//			Thread.sleep(16);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}
//		sensorX = event.values[0];
//		sensorY = event.values[1];
//		sensorZ = event.values[2];
//		
//		Log.i("SensorX" , "x : "+sensorX);
//		Log.i("SensorY" , "y : "+sensorY);
//		Log.i("SensorZ" , "z : "+sensorZ);
//		
//	}
//
//	@Override
//	public boolean onTouch(View v, MotionEvent event) {
//		
//		switch(event.getAction()){
//			
//		case MotionEvent.ACTION_DOWN :
//			if(isZombieShowing)
//			{
//				Toast.makeText(this, "Zombie Killed", Toast.LENGTH_SHORT).show();
//				canvas.drawRGB(200, 0, 0);
//				zombieInit();
//			}
//		}
//		
//		return false;
//	}
//	
//	public void zombieInit(){
//		
//		// Method to initialise zombie at a particular position
//		
//		Random random = new Random();
//		posX = (-3 + 6*random.nextFloat());
//		posY = (-3 + 6*random.nextFloat());
//		posZ = (-3 + 6*random.nextFloat());
//		
//	}
//	
//	public boolean isClose(){
//		
//		//Method to check if the screen orientation 
//		//matches the position of the zombie
//		
//		boolean isCloseX =false;
//		boolean isCloseY =false;
//		boolean isCloseZ =false;
//		
//		if(Math.abs(posX-sensorX)<=2)
//			isCloseX =true;
//		if(Math.abs(posY-sensorY)<=2)
//			isCloseY =true;
//		if(Math.abs(posZ-sensorZ)<=2)
//			isCloseZ =true;
//		
//		if(isCloseX && isCloseY && isCloseZ){
//			return true;
//		}else {
//		return false;
//		}
//	}
//	
//	public void zombieKill(){
//		
//		Toast.makeText(this, "Zombie Killed", Toast.LENGTH_SHORT).show();
//		canvas.drawRGB(200, 0, 0);
//		zombieInit();
//		
//	}
//	
//	 public static boolean isFiring(Context context) {
//	        Intent intent = context.registerReceiver(null, 
//	        		new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
//	        int plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
//	        return plugged == BatteryManager.BATTERY_PLUGGED_AC 
//	        		|| plugged == BatteryManager.BATTERY_PLUGGED_USB;
//	    }
//	
//
//}