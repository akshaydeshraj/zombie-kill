package in.co.nebulaX.foofighter;

import java.io.IOException;
import java.util.Random;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

public class SensorActivity extends Activity implements 
											SensorEventListener , OnTouchListener{

	SensorManager mSensorManager;
	MyCanvas ourSurfaceView;

	Sensor accelerometer;
	Sensor magnetometer;

	float azimuth,pitch , roll;

	Bitmap zombie;

	float[] mGravity;
	float[] mGeomagnetic;

	float pos1a , pos1b , pos1c , pos2a ,pos2b ,pos2c ; 
	Random random;

	Canvas canvas;

	public boolean isZombieShowing;
	
	public class MyCanvas extends SurfaceView implements Runnable , SurfaceHolder.Callback   {

		SurfaceHolder holder;
		Thread thread = null;
		boolean isRunning = false;
		Camera mCamera;
		
		public MyCanvas(Context context) {
			super(context);
			holder = getHolder();
			holder.addCallback(this);
			holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		}

		public void pause() {
			isRunning = false;
			while (true) {
				try {
					thread.join();
					break;
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			thread = null;
		}

		public void resume() {
			isRunning = true;
			thread = new Thread(this);
			thread.start();
		}

		@Override
		public void run() {

			while (isRunning) {
				if (!holder.getSurface().isValid())
					continue;

				canvas = holder.lockCanvas();
				canvas.drawRGB(0, 0, 0);
				isZombieShowing = false;
				
					if (isClose()) {
					canvas.drawBitmap(zombie, 0, 0, null);
					isZombieShowing =true;
				}
				holder.unlockCanvasAndPost(canvas);
			}
		}

		@Override
		public void surfaceChanged(SurfaceHolder holder, int format, int width,
				int height) {
			// TODO Auto-generated method stub
			
			   Camera.Parameters parameters = mCamera.getParameters(); 
		        //parameters.setPreviewSize(w, h); 
		        mCamera.setParameters(parameters); 
		        mCamera.startPreview(); 
		}

		@Override
		public void surfaceCreated(SurfaceHolder mholder) {
			// TODO Auto-generated method stub
			
			mCamera = Camera.open(); 
			mCamera.setDisplayOrientation(90);
	        try {
				mCamera.setPreviewDisplay(mholder);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}

		@Override
		public void surfaceDestroyed(SurfaceHolder holder) {
			// TODO Auto-generated method stub
		
			 mCamera.stopPreview(); 
		        mCamera = null; 
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		  getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN , 
				  WindowManager.LayoutParams.FLAG_FULLSCREEN);
		  
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		if (mSensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER).size() != 0
				&& mSensorManager.getSensorList(Sensor.TYPE_MAGNETIC_FIELD)
						.size() != 0) {
			accelerometer = mSensorManager
					.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
			magnetometer = mSensorManager
					.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		} else {
			Toast.makeText(this, "Hardware not supported", Toast.LENGTH_SHORT)
					.show();
		}
		random = new Random();
		zombieInit();
		zombie = BitmapFactory
				.decodeResource(getResources(), R.drawable.zombie);

		ourSurfaceView = new MyCanvas(this);
		ourSurfaceView.setOnTouchListener(this);
		ourSurfaceView.resume();
		setContentView(ourSurfaceView);
	}

	@Override
	protected void onPause() {
		
		super.onPause();
		mSensorManager.unregisterListener(this);
	}

	@Override
	protected void onResume() {
	
		super.onResume();
		mSensorManager.registerListener(this, accelerometer,
				SensorManager.SENSOR_DELAY_UI);
		mSensorManager.registerListener(this, magnetometer,
				SensorManager.SENSOR_DELAY_UI);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sensor, menu);
		return true;
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		

		try {
			Thread.sleep(16);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
			mGravity = event.values;
		if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
			mGeomagnetic = event.values;
		if (mGravity != null && mGeomagnetic != null) {
			float R[] = new float[9];
			float I[] = new float[9];
			boolean success = SensorManager.getRotationMatrix(R, I, mGravity,
					mGeomagnetic);
			if (success) {
				float orientation[] = new float[3];
				SensorManager.getOrientation(R, orientation);

				azimuth = orientation[0];
				pitch = orientation[1];
				roll = orientation[2];

				// Log.v("orientation", "azimuth :" + (int) (azimuth * 1000));
				// Log.v("orientation" , "pitch :"+pitch);
				// Log.v("orientation", "roll :" + (int) (roll * 1000));
				// orientation contains: azimut, pitch and roll
			}
		}
	}

	public void zombieInit() {
		// To initialise a zombie at a random azimuth and roll position

		pos1a = (-3 + 6 * random.nextFloat());
		pos1b = (-3 + 6 * random.nextFloat());
		pos1c = (-3 + 6 * random.nextFloat());
		
		pos2a = (-3 + 6 * random.nextFloat());
		pos2b = (-3 + 6 * random.nextFloat());
		pos2c = (-3 + 6 * random.nextFloat());
		
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		
		switch(event.getAction()){
			
		case MotionEvent.ACTION_DOWN :
			if(isZombieShowing)
			{
				Toast.makeText(this, "Zombie Killed", Toast.LENGTH_SHORT).show();
				canvas.drawRGB(200, 0, 0);
				zombieInit();
			}
		}
		return false;		
	}
	
	public boolean isClose(){

		//TODO : add queue conditions 
		int counter1=0 , counter2 = 0;
		float error = 0.5f;
		
		if(Math.abs(Math.abs(pos1a)-Math.abs(azimuth))<=error){
			counter1++;
		}
		if(Math.abs(Math.abs(pos1b)-Math.abs(pitch))<=error){
			counter1++;
		}
		if(Math.abs(Math.abs(pos1c)-Math.abs(roll))<=error){
			counter1++;
		}
		
		if(Math.abs(Math.abs(pos2a)-Math.abs(azimuth))<=error){
			counter2++;
		}
		if(Math.abs(Math.abs(pos2b)-Math.abs(pitch))<=error){
			counter2++;
		}
		if(Math.abs(Math.abs(pos2c)-Math.abs(roll))<=error){
			counter2++;
		}
				
		if(counter1==2 || counter2==2){
		return true;
		}else{
			return false;
		}
	}
}